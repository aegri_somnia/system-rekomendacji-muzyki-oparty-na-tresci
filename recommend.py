import glob
import gzip
import os
import pickle
import subprocess
from typing import AnyStr
from playsound import playsound
import numpy as np
import pandas as pd

from extractor import compute_features
# noinspection PyUnresolvedReferences
from hash import LocalitySensitiveHash, LocalitySensitiveHashTable
from utils.utils import DATASET_SIZE


def prepare_tracks(filenames: list[AnyStr], participant_name: str):
    for filename in filenames:
        subprocess.run(
            [
                "sox",
                filename,
                "-C",
                "256",
                os.path.join(
                    f"test_files_trim/{participant_name}",
                    f"{filename[12+len(participant_name):]}",
                ),
                "trim",
                "0",
                "25.0",
            ]
        )


def get_recommended_track_genres() -> pd.DataFrame:
    tracks_data = pd.read_parquet(path="tracks.pq", engine="fastparquet")
    tracks_frame = tracks_data.loc[:, "track"]
    tracks_frame = tracks_frame.drop(
        [
            "bit_rate",
            "comments",
            "composer",
            "date_created",
            "date_recorded",
            "duration",
            "favorites",
            "genres",
            "genres_all",
            "information",
            "interest",
            "language_code",
            "license",
            "listens",
            "lyricist",
            "number",
            "publisher",
            "tags",
        ],
        axis=1,
    )
    tracks_frame.loc[:, "title"] = (
        tracks_data.loc[:, ("artist", "name")] + " - " + tracks_frame.loc[:, "title"]
    )
    return tracks_frame


def recommend_tracks(
    depth: int = 5,
    filenames: list[AnyStr] = None,
    lsh: LocalitySensitiveHash = None,
    counter: int = 1000000,
    participant_name: str = "",
) -> dict:
    recommendations = dict()
    for filename in filenames:
        features = compute_features(
            track_id=counter, dataset=False, filepath=filename
        ).values[0]
        pad = 1
        lsh.add(index=counter, vector=features)
        recommendations[filename[17 + len(participant_name) :]] = lsh.query(
            vector=features, depth=depth + pad, index=counter
        )
        counter += 1
    return recommendations


def play_recommendations(
    participant_name: str,
    recommendations: dict,
    tracks_info: pd.DataFrame,
    recommendation_feedback: pd.DataFrame,
):
    i = 0
    for track, recommendations_array in recommendations.items():
        print(track, recommendations_array)
        j = 0
        for recommendation, n_bins in recommendations_array:
            # This means that the most similar track is the track itself
            if recommendation > tracks_info.index.max():
                pass
            else:
                print(f"Original track: {track}")
                print(
                    f"Recommendation title: {tracks_info.loc[recommendation,'title']}\nGenre: {tracks_info.loc[recommendation,'genre_top']}"
                )
                track_id_string = f"{recommendation:06}"

                recommendation_feedback["original_track_name"][
                    participant_name + f"{DATASET_SIZE}_{i}_{j}"
                ] = track
                recommendation_feedback["recommended_track_id"][
                    participant_name + f"{DATASET_SIZE}_{i}_{j}"
                ] = track_id_string
                recommendation_feedback["recommended_track_name"][
                    participant_name + f"{DATASET_SIZE}_{i}_{j}"
                ] = tracks_info.loc[recommendation, "title"]
                recommendation_feedback["recommended_track_genre"][
                    participant_name + f"{DATASET_SIZE}_{i}_{j}"
                ] = tracks_info.loc[recommendation, "genre_top"]
                try:
                    playsound(
                        sound=os.path.join(
                            f"fma_{DATASET_SIZE}_trim",
                            f"{track_id_string[:3]}",
                            f"{track_id_string}.mp3",
                        )
                    )
                except Exception as e:
                    print(f"Recommendation failed with: {e}")
                while True:
                    try:
                        recommendation_feedback["recommendation_score"][
                            participant_name + f"{DATASET_SIZE}_{i}_{j}"
                        ] = int(
                            input(
                                "Czy rekomendacja była dobra? Podaj odpowiedź w skali 1-5:\n"
                            )
                        )
                        break
                    except:
                        print("Podaj odpowiedź tylko w  skali 1-5!")
                while True:
                    try:
                        recommendation_feedback["recommendation_expectability"][
                            participant_name + f"{DATASET_SIZE}_{i}_{j}"
                        ] = int(
                            input(
                                "Czy rekomendacja była zaskakująca? Podaj odpowiedź w skali 1-5:\n"
                            )
                        )
                        break
                    except:
                        print("Podaj odpowiedź tylko w skali 1-5!")
                j += 1
                input("Press any key to go to the next recommended track.")
        i += 1
        input("Press any key to go to the next track and it's recommendations.")


def main():
    pd.set_option("mode.chained_assignment", None)
    with gzip.open(filename=f"lsh_{DATASET_SIZE}_trim.p", mode="rb") as f:
        p = pickle.Unpickler(f)
        lsh: LocalitySensitiveHash = p.load()

    tracks_info = get_recommended_track_genres()

    participant_name = input("Enter participant name: ")
    filenames = glob.glob(f"test_files/{participant_name}/*")
    prepare_tracks(filenames=filenames, participant_name=participant_name)
    filenames_trim = glob.glob(f"test_files_trim/{participant_name}/*")
    depth = 5
    counter = tracks_info.index.max()
    recommendations = recommend_tracks(
        depth=depth,
        filenames=filenames_trim,
        lsh=lsh,
        counter=counter,
        participant_name=participant_name,
    )

    recommendation_feedback = (
        pd.read_parquet(path=f"{DATASET_SIZE}_feedback.pq", engine="fastparquet")
        if os.path.exists(f"{DATASET_SIZE}_feedback.pq")
        else pd.DataFrame(
            columns=[
                "original_track_name",
                "recommended_track_id",
                "recommended_track_name",
                "recommended_track_genre",
                # czy rekomendacja była dobra (akceptowalna?)
                "recommendation_score",
                # czy była spodziewana/zaskakująca/nowa?
                "recommendation_expectability",
            ]
        )
    )
    for i in range(5):
        for j in range(5):
            recommendation_feedback.loc[participant_name + f"{DATASET_SIZE}_{i}_{j}"] = np.NaN
    print(recommendation_feedback.head(n=20))
    play_recommendations(
        participant_name=participant_name,
        recommendations=recommendations,
        tracks_info=tracks_info,
        recommendation_feedback=recommendation_feedback,
    )
    print(recommendation_feedback.tail())
    recommendation_feedback.to_parquet(
        path=f"{DATASET_SIZE}_feedback.pq",
        engine="fastparquet",
        index=True,
        append=True if os.path.exists(f"{DATASET_SIZE}_feedback.pq") else False,
        compression="ZSTD",
    )


if __name__ == "__main__":
    main()

import gc
import gzip
import pickle
import pickletools

import numpy as np
import pandas as pd
from tqdm import tqdm

from utils.utils import DATASET_SIZE


class LocalitySensitiveHashTable:
    def __init__(self, dimensions: int, hash_size: int):
        self.dimensions = dimensions
        self.hash_size = hash_size
        self.hash_table = dict()
        self.projection = np.random.randn(self.hash_size, self.dimensions)

    def hash(self, vector: np.ndarray) -> str:
        bool_vector = np.dot(vector, self.projection.T) > 0
        return "".join(bool_vector.astype(int).astype(str))

    def add(self, vector: np.ndarray, index: int):
        hashed_item = self.hash(vector=vector)
        if hashed_item not in self.hash_table:
            self.hash_table[hashed_item] = [{"index": index}]
        else:
            self.hash_table[hashed_item].append({"index": index})

    def get(self, vector: np.ndarray) -> list | dict:
        hashed_item = self.hash(vector=vector)
        if hashed_item not in self.hash_table:
            return list()
        else:
            return self.hash_table[hashed_item]


class LocalitySensitiveHash:
    def __init__(self, dimensions: int, hash_size: int = 16):
        self.hash_size = hash_size
        self.num_tables = 50
        self.tables = list()
        for i in range(self.num_tables):
            self.tables.append(
                LocalitySensitiveHashTable(
                    dimensions=dimensions, hash_size=self.hash_size
                )
            )

    def add(self, vector: np.ndarray, index: int):
        for hash_table in self.tables:
            hash_table.add(vector=vector, index=index)

    def get(self, vector: np.ndarray) -> list:
        results = list()
        for hash_table in self.tables:
            results.extend(hash_table.get(vector=vector))
        return results

    def query(self, vector: np.ndarray, depth: int, index: int) -> list:
        counts = dict()
        for result in self.get(vector=vector):
            if result["index"] in counts:
                counts[result["index"]] += 1
            else:
                counts[result["index"]] = 1
        results = []
        for counter, r in enumerate(sorted(counts, key=counts.get, reverse=True)):
            if r != index:
                results.append([r, counts[r]])
                counter += 1
            if counter == depth:
                del counts
                gc.collect()
                return results


def create_lsh(files_list: np.ndarray):
    tracks = pd.read_parquet(
        path=f"features_{DATASET_SIZE}_trim.pq", engine="fastparquet"
    )
    track_features = dict()
    for file in files_list:
        try:
            track_features[file] = tracks.loc[file].to_numpy()
        except Exception as e:
            print(f"Failed with error: {e}")
    lsh = LocalitySensitiveHash(dimensions=9, hash_size=128)
    for index, vector in tqdm(iterable=track_features.items(), total=len(files_list)):
        lsh.add(index=index, vector=vector)

    with gzip.open(filename=f"lsh_{DATASET_SIZE}_trim.p", mode="wb") as f:
        pickled = pickle.dumps(lsh, pickle.HIGHEST_PROTOCOL)
        optimized_pickle = pickletools.optimize(pickled)
        f.write(optimized_pickle)


def main():
    files_list: np.ndarray = np.load(file=f"tracks_{DATASET_SIZE}.npy")
    create_lsh(files_list=files_list)


if __name__ == "__main__":
    main()

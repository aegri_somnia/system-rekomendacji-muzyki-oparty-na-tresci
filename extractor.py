#!/usr/bin/env python3
import gc
import os
import shutil
import time
from multiprocessing import get_context
from typing import Any

import essentia.streaming
import librosa
import numpy as np
import pandas as pd
import psutil
from librosa import feature
from pandas import DataFrame
from tqdm import tqdm

from utils.utils import setup_logger, DATASET_SIZE, DEBUG

SAMPLING_RATE = 44100

logger = setup_logger("thesis")
if DEBUG:
    essentia.log.infoActive = True
    essentia.log.debugLevels += essentia.EAll  # activate all debug modules


def load_track(
    track_id: int, dataset: bool = True, filepath: str = None
) -> [np.ndarray, Any]:
    if dataset:
        # padding track id with zeros
        track_id_string = f"{track_id:06}"
        # return loaded file for librosa as first argument, for essentia as second
        return (
            librosa.load(
                path=os.path.join(
                    f"fma_{DATASET_SIZE}_trim",
                    f"{track_id_string[:3]}",
                    f"{track_id_string}.mp3",
                ),
                sr=SAMPLING_RATE,
                res_type="kaiser_fast",
            )[0],
            essentia.streaming.MonoLoader(
                filename=os.path.join(
                    f"fma_{DATASET_SIZE}_trim",
                    f"{track_id_string[:3]}",
                    f"{track_id_string}.mp3",
                )
            ),
        )
    else:
        return (
            librosa.load(
                path=filepath,
                sr=SAMPLING_RATE,
                res_type="kaiser_fast",
            )[0],
            essentia.streaming.MonoLoader(filename=filepath),
        )


def essentia_preparation() -> Any:
    return (
        essentia.streaming.FrameCutter(),
        essentia.streaming.Windowing(type="blackmanharris62"),
        essentia.streaming.Spectrum(),
        essentia.streaming.SpectralPeaks(
            orderBy="frequency",
            magnitudeThreshold=0.00001,
            minFrequency=20,
            maxFrequency=20000,
            maxPeaks=60,
        ),
        essentia.Pool(),
    )


# noinspection PyStatementEffect
def compute_features(
    track_id: int, dataset: bool = True, filepath: str = None
) -> DataFrame:
    features = pd.DataFrame(
        index=[track_id],
        columns=pd.Index(
            dict(
                # from librosa
                spectral_centroid=1,
                spectral_rolloff=1,
                zero_crossing_rate=1,
                bpm=1,
                # from essentia
                spectral_complexity=1,
                dissonance=1,
                danceability=1,
                onset_rate=1,
                inharmonicity=1,
            )
        ),
        dtype=np.float16,
    )
    try:
        y, loader = load_track(track_id=track_id, dataset=dataset, filepath=filepath)

        features.at[track_id, "spectral_centroid"] = np.average(
            librosa.feature.spectral_centroid(y=y, sr=SAMPLING_RATE)
        )

        features.at[track_id, "spectral_rolloff"] = np.average(
            librosa.feature.spectral_rolloff(y=y, sr=SAMPLING_RATE)
        )

        features.at[track_id, "zero_crossing_rate"] = np.average(
            librosa.feature.zero_crossing_rate(y=y)
        )

        features.at[track_id, "bpm"] = librosa.feature.tempo(y=y, sr=SAMPLING_RATE)
        del y
        (
            framecutter,
            window,
            spectrum,
            spectralpeaks,
            pool,
        ) = essentia_preparation()
        dissonance = essentia.streaming.Dissonance()
        danceability = essentia.streaming.Danceability()
        onset_rate = essentia.streaming.OnsetRate()
        inharmonicity = essentia.streaming.Inharmonicity()
        spectral_complexity = essentia.streaming.SpectralComplexity()

        loader.audio >> framecutter.signal
        framecutter.frame >> window.frame >> spectrum.frame
        spectrum.spectrum >> spectralpeaks.spectrum

        spectralpeaks.magnitudes >> dissonance.magnitudes
        spectralpeaks.frequencies >> dissonance.frequencies
        dissonance.dissonance >> (pool, "tonal.dissonance")

        spectralpeaks.magnitudes >> inharmonicity.magnitudes
        spectralpeaks.frequencies >> inharmonicity.frequencies
        inharmonicity.inharmonicity >> (pool, "tonal.inharmonicity")

        loader.audio >> danceability.signal
        danceability.danceability >> (pool, "rhythm.danceability")
        danceability.dfa >> None

        loader.audio >> onset_rate.signal
        onset_rate.onsetTimes >> None
        onset_rate.onsetRate >> (pool, "rhythm.onsetRate")

        spectrum.spectrum >> spectral_complexity.spectrum
        spectral_complexity.spectralComplexity >> (pool, "spectral.spectralComplexity")

        essentia.run(loader)
        del (
            loader,
            dissonance,
            danceability,
            onset_rate,
            framecutter,
            window,
            spectrum,
            spectralpeaks,
            spectral_complexity,
        )
        features.at[track_id, "dissonance"] = np.average(pool["tonal.dissonance"])

        features.at[track_id, "danceability"] = pool["rhythm.danceability"]

        features.at[track_id, "onset_rate"] = pool["rhythm.onsetRate"]

        features.at[track_id, "inharmonicity"] = np.average(pool["tonal.inharmonicity"])

        features.at[track_id, "spectral_complexity"] = np.average(
            pool["spectral.spectralComplexity"]
        )
        del pool
        gc.collect()
    except Exception as e:
        print(f"Track with id: {track_id} failed with error: {e}")
        logger.error(
            msg=f"Track with id: {track_id} failed with error: {e}", exc_info=True
        )
    return features


def main():
    # we split all the track id's into equal length chunks so if the processing fails we still have part of the job done
    track_ids_chunks = np.array_split(
        # we check if there's already part of the data calculated, if not we import index of all the files in the
        # dataset
        ary=np.setdiff1d(
            ar1=np.load(f"tracks_{DATASET_SIZE}.npy"),
            ar2=np.load(f"tracks_{DATASET_SIZE}_completed.npy"),
        )
        if os.path.exists(f"tracks_{DATASET_SIZE}_completed.npy")
        else np.load(f"tracks_{DATASET_SIZE}.npy"),
        indices_or_sections=len(
            np.setdiff1d(
                ar1=np.load(f"tracks_{DATASET_SIZE}.npy"),
                ar2=np.load(f"tracks_{DATASET_SIZE}_completed.npy"),
            )
        )
        // 1000
        if os.path.exists(f"tracks_{DATASET_SIZE}_completed.npy")
        else len(np.load(f"tracks_{DATASET_SIZE}.npy")) // 1000,
    )
    for i, chunk in enumerate(
        iterable=tqdm(
            iterable=track_ids_chunks,
            total=len(track_ids_chunks),
            desc=f"Start time -> {time.ctime(time.time())} Main loop: ",
        )
    ):
        # get_context("spawn"), because forking processes in linux doesn't copy all the data and process can deadlock
        with get_context("spawn").Pool(
            processes=psutil.cpu_count(logical=False), maxtasksperchild=10
        ) as process_pool:
            tracks = []
            for j, track in enumerate(
                tqdm(
                    iterable=process_pool.imap_unordered(
                        compute_features, chunk, chunksize=10
                    ),
                    total=len(chunk),
                    desc=f"Start time -> {time.ctime(time.time())} Loop for chunk {i}",
                )
            ):
                tracks.append(track)
                if j % 50 == 0 or j == len(chunk) - 1:
                    np.save(
                        arr=np.append(
                            arr=np.load(f"tracks_{DATASET_SIZE}_completed.npy"),
                            values=[track.index.values[0] for track in tracks],
                        )
                        if os.path.exists(f"tracks_{DATASET_SIZE}_completed.npy")
                        else np.array([track.index.values[0] for track in tracks]),
                        file=f"tracks_{DATASET_SIZE}_completed.npy",
                    )

                    tracks = pd.concat(tracks)

                    tracks.to_parquet(
                        path=f"features_{DATASET_SIZE}_trim.pq",
                        engine="fastparquet",
                        index=True,
                        append=True
                        if os.path.exists(f"features_{DATASET_SIZE}_trim.pq")
                        else False,
                        # pip install zstandard
                        compression="ZSTD",
                    )
                    tracks = []
                del track
                gc.collect()
        if i % 10 == 0 and i != 0:
            # backup the file after couple successful chunks in case it gets corrupted
            if shutil.disk_usage(os.curdir).free > os.path.getsize(
                f"features_{DATASET_SIZE}_trim.pq"
            ):
                # but check if there's free space available
                shutil.copy2(
                    src=f"features_{DATASET_SIZE}_trim.pq",
                    dst=f"features_{DATASET_SIZE}_trim_{time.strftime('%Y%m%d-%H%M%S')}.pq",
                )
            else:
                print("Not enough space on disk!")


if __name__ == "__main__":
    main()

import ast
import glob
import logging
import os
import subprocess
import time

import numpy as np
import pandas as pd
from tqdm import tqdm

DATASET_SIZE = "small"
DEBUG = False


def setup_logger(logger_name: str) -> logging.Logger:
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename="thesis.log", encoding="utf-8", mode="a")
    handler.setFormatter(
        logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
    )
    logger.addHandler(handler)
    return logger


def load_tracks_index(path) -> np.ndarray:
    return pd.read_parquet(
        path=path,
        engine="fastparquet",
        columns=[],
    ).index.values.astype(np.int32)


def convert_source_csv_to_pq_file(csv_filepath) -> None:
    tracks = pd.read_csv(csv_filepath, index_col=0, header=[0, 1])

    COLUMNS = [
        ("track", "tags"),
        ("album", "tags"),
        ("artist", "tags"),
        ("track", "genres"),
        ("track", "genres_all"),
    ]
    for column in COLUMNS:
        tracks[column] = tracks[column].map(ast.literal_eval)

    COLUMNS = [
        ("track", "date_created"),
        ("track", "date_recorded"),
        ("album", "date_created"),
        ("album", "date_released"),
        ("artist", "date_created"),
        ("artist", "active_year_begin"),
        ("artist", "active_year_end"),
    ]
    for column in COLUMNS:
        tracks[column] = pd.to_datetime(tracks[column])

    COLUMNS = [
        ("track", "genre_top"),
        ("track", "license"),
        ("album", "type"),
        ("album", "information"),
        ("artist", "bio"),
    ]
    for column in COLUMNS:
        tracks[column] = tracks[column].astype("category")
    # remove the failed tracks
    tracks = tracks[~tracks.index.isin(np.load("failed.npy"))]
    tracks.to_parquet(path="tracks.pq", index=True)


def trim_all_dataset_to_25s(dataset_name: str):
    tracks = np.load("tracks.npy")
    # files that failed to load using librosa during testing
    failed = np.load(file="failed.npy")
    print(f"Start time -> {time.ctime(time.time())}")
    for track in tqdm(iterable=tracks, total=len(tracks)):
        if track not in failed:
            track_id_string = f"{track:06}"
        try:
            subprocess.run(
                [
                    "sox",
                    # original dataset
                    os.path.join(
                        dataset_name,
                        f"{track_id_string[:3]}",
                        f"{track_id_string}.mp3",
                    ),
                    "-C",
                    "256",
                    # trimmed one
                    os.path.join(
                        f"{dataset_name}_trim",
                        f"{track_id_string[:3]}",
                        f"{track_id_string}.mp3",
                    ),
                    "trim",
                    "0",
                    "25.0",
                ]
            )
        except Exception as e:
            print(e)
            pass


def get_all_files_in_directory(directory: str):
    np.save(
        file=f"tracks_{DATASET_SIZE}.npy",
        arr=np.asarray([int(x[-10:-4]) for x in glob.glob(f"{directory}/*/*")]),
    )
